## Introduction

A job processing framework built on top of beanstalkd.

What makes it different?  R2Q2 Is intended to have a gnarly configuration file, but then largely self-managed: fire and forget.

It is built for rails environments.

## Installation

    gem 'r2q2', :git => "git://github.com/bdigital/r2q2.git"

Be sure to install beanstalkd.  On ubuntu this might look like...

     apt-get install redis-server beanstalkd.

## Basic Usage 

A few moving parts to this.

1.  Defining Jobs
2.  R2Q2 Configuration
3.  Scheduling Jobs
4.  Processing Jobs
  
### Defining Jobs

In rails environment perhaps lib/jobs/process_data.rb

      class Jobs::ProcessData < R2Q2::Job
  
        attr_accessor :activel_record_model, :other_variable

        def perform
          active_record_model.process_data
          # Some other stuff
        end
      end

Note that the job class inherits from R2Q2::Job and implements perform.  Note active_record_model attribute in this case is probably a Marshalled Active Record object.  Convenient!

### Configuration

In a rails initializer, perhaps named /config/initializers/r2q2.rb

      R2Q2.config do |config|
        config.max_processes = 3
        config.noisy = true #  Perhaps set to false for production
  
        config.register({:class => Jobs::SendEmail, :tube => 'email', :priority => 1})
        config.register({:class => Jobs::GithubSync, :tube => 'githubsync', :priority => 2})
        config.register({:class => Jobs::TrainSVM, :tube => 'svm', :priority => 2})
  
        config.control_proc = Proc.new do
          # Do something normally handed off to cron
          # Do something which checks state and does something else
        end
      end

So what is all this?

max_processes attribute will tell the r2q2 worker process the maximum forks at any one time.

noisy will result in a status stream being printed to stdout.

the register commands set how specific job types (implemented as above) will be treated. 

the control_proc is a poor man's cron.  at a configurable time interval it will be called.

### Scheduling Jobs
In application Code
    
      Job::SendEmail.new(:recipient => "berrydigital@gmail.com", :subject => "background job.").enqueue

Note the parameters to new must be matched by attributes in the job class implementation.

### Processing Jobs

In your shell just invoke the rake task that comes with the library

rake r2q2:start

the result is that this process will handle jobs according to the rules you setup.


