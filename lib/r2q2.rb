#
# Background job framework
#
require 'r2q2/configuration'
require 'r2q2/worker'
require 'r2q2/registered_job'
require 'r2q2/core'
require 'r2q2/job'
require 'r2q2/railtie' if defined?(Rails) and Rails::VERSION::MAJOR > 2

module R2Q2
end
