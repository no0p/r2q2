require 'rake'
require 'r2q2'

namespace :r2q2 do
  desc "Start a r2q2 worker process.  Note, do this once and rely on configuration."
  task :start => :environment do
    R2Q2.dispatch_loop
  end
end
