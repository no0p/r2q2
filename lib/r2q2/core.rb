module R2Q2

  @@config = Configuration.new
  
  @@beanstalk = nil
  @@redis = nil
  
  @@workers = []
  
  @@halt = false

  #
  # Establish a conection to beanstalk server
  #
  def self.config(&block)
    yield @@config
    update_connections
  end
  

  
  #
  # Add a job to the queue for processing
  #
  def self.add(job)
    registered_job = @@config.registered_jobs.detect {|rj| rj.class.to_s == job.class.to_s}
    if registered_job.blank?
      raise "trying to schedule an unregistered job!!"
    else
      @@beanstalk.use registered_job.tube
      @@beanstalk.put(Marshal.dump(job))
    end
  end
  
  #
  # A never ending loop which polls around for state of the queues, and 
  #   forks necessary workers, updates state, etc.
  # 
  #
  def self.dispatch_loop
    trap('INT') { puts "exiting"; @@halt = true }
    trap('TERM') { puts "exiting"; @@halt = true }
    cycles = 0
    while true do
      begin
        Process.exit unless !@@halt
        needed_workers.each do |registered_job| 
          conf = ActiveRecord::Base.remove_connection
          pid = fork do
            ActiveRecord::Base.establish_connection conf
            @@worked_jobs = 0
            work registered_job
          end
          ActiveRecord::Base.establish_connection conf
          @@workers.push Worker.new(registered_job, pid)
          Process.detach pid
          $stdout.print "*" unless !@@config.noisy
        end
        
        cycles += 1
        if @@config.control_cycles <= cycles
          pid = fork do
            @@config.control_proc.call
            Process.exit
          end
          Process.detach pid
          cycles = 0  
        end
        
        sleep 1 unless needed_workers.length > 0
        
      rescue StandardError => e
        puts e.to_s + e.backtrace.join("\n\t")
        update_connections
      end      
    end
  end
  
  #
  # Work a tube loop; a forked process which actually processes jobs until interrupted.  
  #
  def self.work(rj)
    update_connections
    
    @@beanstalk.watch rj.tube
    while true do
    
      if @@halt || @@worked_jobs >= rj.max_units_per_fork
        Process.exit
      end
    
      begin
        job = @@beanstalk.reserve(1)
        $stdout.print "<#{rj.verbose_flag}" unless !@@config.noisy
        process job
        @@worked_jobs += 1
        job.delete
        $stdout.print ">" unless !@@config.noisy 
      rescue Beanstalk::TimedOut
        #$stderr.puts "Beanstalk Timeout"
        job.bury unless job.nil?
        Process.exit!
      rescue StandardError => e
        puts e.to_s + e.backtrace.join("\n\t")
        if !job.nil?
          puts "BURYING"
          job.bury
          puts job.body
        end
      end
    end
  end
  
  #
  # Given the state of the system, which tubes should workers be launched for.
  #
  def self.needed_workers
    workers_to_launch = []
    
    # Prune failed workers
    @@workers.each do |w|
      begin
        Process.getpgid(w.pid)
      rescue Errno::ESRCH
        @@workers.delete w    # TODO Is there a better approach?
      end
    end
    
    # Evaluate tube queue lengths
    scores = {}
    @@beanstalk.list_tubes.values.flatten.each do |t|
      stats = @@beanstalk.stats_tube t
      rj = @@config.registered_jobs.detect {|rjob| rjob.tube == t}
      scores[rj] = stats["current-jobs-ready"] unless rj.nil? #TODO starting off with naive approach.
    end

    # Optimize
    #  TODO determine preffered grouping of processes
    scores.sort_by {|a, b| b}.each do |ts|
      if @@workers.length + workers_to_launch.length <= @@config.max_processes
        workers_to_launch.push ts.first unless ts.last == 0 # zero in queue score
      end
    end

    return workers_to_launch
  end
  
  
  def self.configuration
    return @@config
  end
  
  def self.beanstalk_connection
    return @@beanstalk
  end
  
  def self.redis_connection
    return @@redis
  end
  
  class UnknownJobType < Exception
  end
  
  class InvalidJobFormat < Exception
  end


  #
  # Re-establishing connections
  #
  def self.update_connections
    @@beanstalk.close unless @@beanstalk.nil?
    @@beanstalk = Beanstalk::Pool.new(["#{@@config.beanstalk_server}:#{@@config.beanstalk_port}"])
  end
  
  #
  # Load a job and invoke perform
  #
  def self.process(job)
    job = Marshal.load job.body
    raise UnknownJobType unless @@config.registered_jobs.detect {|rj| rj.class = job.class }
    job.perform
  end
  
 
end
