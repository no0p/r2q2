# Cargo Culted from Texticle.
# Module used to conform to Rails 3 plugin API
require File.expand_path( File.dirname(__FILE__) + '/../r2q2')

module R2Q2
  class Railtie < Rails::Railtie
    rake_tasks do
      load File.dirname(__FILE__) + '/tasks.rb'
    end
  end
end

