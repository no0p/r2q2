#
# Represents a job registered to be run.  Includes parameters for weighting priority.
#
module R2Q2
  class RegisteredJob
    
    attr_accessor :priority, :max_units_per_fork, :class, :tube, :verbose_flag
    
    def initialize(params = {})
      raise "parameters must be hash" unless params.is_a?(Hash)
      if params[:tube].blank? || !params[:tube].is_a?(String)
        raise "tube must be a nonblank string"
      end
      if params[:class].blank? || !params[:class].is_a?(Class)
        raise "class must be included"
      end
      rjconf = {:priority => 3, :max_units_per_fork => 50, :verbose_flag => params[:tube][0]}
      rjconf.merge! params
      rjconf.each {|k, v| self.send k.to_s + "=", v}
    end
    
  end
end
