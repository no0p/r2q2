module R2Q2
  class Configuration
    attr_accessor :beanstalk_server, :beanstalk_port
    attr_accessor :redis_server, :redis_port, :redis_db
    attr_accessor :max_processes
    attr_accessor :registered_jobs, :control_proc, :control_cycles
    attr_accessor :noisy
    
    def initialize
      @beanstalk_server = 'localhost'
      @beanstalk_port = 11300
      @redis_server = 'localhost'
      @redis_port = 6379
      @max_processes = 3
      @noisy = false
      @registered_jobs = []
      
      @control_proc = Proc.new {}
      @control_cycles = 30
    end
    
    #
    # Register a new job type, done in setup once
    #
    def register(parameters)
      @registered_jobs.push RegisteredJob.new(parameters)
    end
    
  end
end
