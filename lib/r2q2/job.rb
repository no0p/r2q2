#
# Background jobs.  Implement as methods and pass to R2 enqueue
#
module R2Q2
  class Job
    
    def initialize(args = {})
      args.each do |k, v|
        self.send k.to_s + "=", v
      end unless !args.is_a? Hash
    end
    
    def enqueue
      R2Q2.add self
      return self
    end
    
    #def self.tube
    #  self.to_s.downcase.gsub(/[^a-zA-Z]/, '')
    #end
    
  end
end
