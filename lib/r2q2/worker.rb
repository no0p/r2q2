#
# A class which represents a process working a tube.
#
module R2Q2
  class Worker
    attr_accessor :launched, :pid, :registered_job
    
    def initialize(reg_job, pid)
      @registered_job, @pid = reg_job, pid
      @launched = Time.now
    end
  end
end
