spec = Gem::Specification.new do |s|
  s.name = 'r2q2'
  s.version = '0.0.1'
  s.date = '2011-07-01'
  s.summary = 'Another job processing framework built on beanstalkd AND redis for rails environments.'
  s.email = "berrydigital@gmail.com"
  s.homepage = "http://github.com/bdigital/r2q2"
  s.description = "Another job processing framwork built on beanstalkd for rails environments."
  s.has_rdoc = false
  s.executables = []
  s.add_dependency('beanstalk-client')
  s.add_dependency('redis')
  s.authors = ["Robert Berry"]
  s.files = ["lib/r2q2.rb", "lib/r2q2/core.rb", "lib/r2q2/job.rb", "lib/r2q2/railtie.rb", "lib/r2q2/configuration.rb", "lib/r2q2/registered_job.rb", "lib/r2q2/worker.rb", "lib/r2q2/railtie.rb", "rails/init.rb", "lib/r2q2/tasks.rb", "README.md", "LICENSE"]
end

